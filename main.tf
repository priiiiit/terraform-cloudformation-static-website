locals {
  tags = merge(var.tags, {
    "terraform:module" = "terraform-cloudformation-static-website"
    Terraform          = "true"
    Changed            = formatdate("YYYY-MM-DD hh:mm ZZZ", timestamp())
  })
}

/* -------------------------- Providers definition -------------------------- */

// Default provider will be inherited from the enclosing configuration
// The provider below is required to handle ACM and Lambda in a CloudFront context
provider "aws" {
  alias   = "us-east-1"
  version = "~> 2.0"
  region  = "us-east-1"
}

/* -------------------------------- Route 53 -------------------------------- */

data "aws_route53_zone" "main" {
  name         = var.route53_zone
  private_zone = false
}

/* ---------------------- ACM (AWS Certificate Manager) --------------------- */

// Creates the certificate for yourdomain.com
resource "aws_acm_certificate" "website" {
  // Wilcard certificate used by CloudFront requires this specific region
  // (https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/cnames-and-https-requirements.html)
  provider          = aws.us-east-1
  domain_name       = var.website_domain
  validation_method = "DNS"

  tags = local.tags

  lifecycle {
    ignore_changes = [tags]
  }
}

// Validates the ACM by creating a Route53 record (as `validation_method` is set to `DNS` in the aws_acm_certificate resource)
resource "aws_route53_record" "cert_validation" {
  name    = aws_acm_certificate.website.domain_validation_options[0].resource_record_name
  type    = aws_acm_certificate.website.domain_validation_options[0].resource_record_type
  zone_id = data.aws_route53_zone.main.zone_id
  records = [aws_acm_certificate.website.domain_validation_options[0].resource_record_value]
  ttl     = "60"
}

// Triggers the ACM certificate validation event
resource "aws_acm_certificate_validation" "cert" {
  provider = aws.us-east-1

  certificate_arn         = aws_acm_certificate.website.arn
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}

// Get the ARN of the issued certificate
data "aws_acm_certificate" "website" {
  provider = aws.us-east-1

  depends_on = [
    aws_acm_certificate.website,
    aws_route53_record.cert_validation,
    aws_acm_certificate_validation.cert,
  ]

  domain      = var.website_domain
  statuses    = ["ISSUED"]
  most_recent = true
}

/* ----------------------------------- S3 ----------------------------------- */

// Creates bucket to store logs
resource "aws_s3_bucket" "website_logs" {
  bucket        = "${var.website_domain}-logs"
  acl           = "log-delivery-write"
  force_destroy = var.force_destroy_bucket

  tags = local.tags

  lifecycle {
    ignore_changes = [tags]
  }
}

// Creates bucket to store the static website
resource "aws_s3_bucket" "website_root" {
  bucket        = "${var.website_domain}-root"
  force_destroy = var.force_destroy_bucket

  logging {
    target_bucket = aws_s3_bucket.website_logs.bucket
    target_prefix = "${var.website_domain}/"
  }

  website {
    index_document = var.index_document
    error_document = var.error_document
  }

  tags = local.tags

  lifecycle {
    ignore_changes = [tags]
  }
}


/* ------------------------------- CloudFront ------------------------------- */

// Creates an Amazon CloudFront origin access identity (will be used in the distribution origin configuration)
resource "aws_cloudfront_origin_access_identity" "origin_access_identity_website" {
  comment = "CloudfrontOriginAccessIdentity - ${var.website_domain}"
}

// Creates the CloudFront distribution to serve the static website
resource "aws_cloudfront_distribution" "website_cdn_root" {
  enabled     = true
  price_class = "PriceClass_All"
  aliases     = [var.website_domain]

  origin {
    origin_id   = "origin-bucket-${aws_s3_bucket.website_root.id}"
    domain_name = aws_s3_bucket.website_root.bucket_regional_domain_name

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity_website.cloudfront_access_identity_path
    }
  }

  default_root_object = var.index_document

  logging_config {
    bucket = aws_s3_bucket.website_logs.bucket_domain_name
    prefix = "${var.website_domain}/"
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "origin-bucket-${aws_s3_bucket.website_root.id}"
    min_ttl          = "0"
    default_ttl      = "300"
    max_ttl          = "1200"

    viewer_protocol_policy = "redirect-to-https"
    compress               = true

    forwarded_values {
      query_string = false
      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = data.aws_acm_certificate.website.arn
    ssl_support_method  = "sni-only"
  }

  custom_error_response {
    error_code            = 404
    error_caching_min_ttl = 0
    response_page_path    = "/${var.error_document}"
    response_code         = 200
  }

  tags = local.tags

  lifecycle {
    ignore_changes = [
      tags,
      viewer_certificate,
    ]
  }
}

// Creates the DNS record to point on the main CloudFront distribution ID
resource "aws_route53_record" "website_cdn_root_record" {
  zone_id = data.aws_route53_zone.main.zone_id
  name    = var.website_domain
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.website_cdn_root.domain_name
    zone_id                = aws_cloudfront_distribution.website_cdn_root.hosted_zone_id
    evaluate_target_health = false
  }
}


// Creates policy to limit access to the S3 bucket to CloudFront Origin
resource "aws_s3_bucket_policy" "update_website_root_bucket_policy" {
  bucket = aws_s3_bucket.website_root.id

  policy = <<POLICY
{
"Version": "2008-10-17",
"Id": "PolicyForCloudFrontPrivateContent",
"Statement": [
{
  "Sid": "AllowCloudFrontOriginAccess",
  "Effect": "Allow",
  "Principal": {
    "AWS": "${aws_cloudfront_origin_access_identity.origin_access_identity_website.iam_arn}"
  },
  "Action": [
    "s3:GetObject",
    "s3:ListBucket"
  ],
  "Resource": [
    "${aws_s3_bucket.website_root.arn}/*",
    "${aws_s3_bucket.website_root.arn}"
  ]
}
]
}
POLICY
}

