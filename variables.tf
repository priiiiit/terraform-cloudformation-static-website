variable "route53_zone" {
  description = "Route53 zone that will host the website"
  type        = string
}

variable "website_domain" {
  description = "Main website domain, e.g. app.foo.bar"
  type        = string
}

variable "force_destroy_bucket" {
  description = "If you are uncomfortable with Terraform destroying the buckets then set it to false"
  default     = true
  type        = bool
}

variable "index_document" {
  description = "Filename for the index document. Usually index.html"
  default     = "index.html"
  type        = string
}
variable "error_document" {
  description = "index.html"
  default     = "Filename for the index document. Can be also set to index.html"
  type        = string
}

variable "tags" {
  description = "Optional tags that will be added to the resources"
  default     = {}
  type        = map
}